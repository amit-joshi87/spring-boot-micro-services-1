package com.amit.springbootmicroservices1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMicroServices1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMicroServices1Application.class, args);
	}
}
